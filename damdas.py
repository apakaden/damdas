from frontend.gui import BoardGUI
from frontend.form_gui import FormGUI
from frontend.ai_accessor import AIAccessor
from frontend.store import Store
from backend.player import Player
from backend.ai import AI
from backend.board import Board
from backend.game import Game


if __name__ == "__main__":
    player_1 = Player("User", [Board.Direction.RIGHT,
                               Board.Direction.UP,
                               Board.Direction.DOWN,
                               Board.Direction.UP_RIGHT,
                               Board.Direction.DOWN_RIGHT])

    player_2 = Player("AI", [Board.Direction.LEFT,
                                Board.Direction.UP,
                                Board.Direction.DOWN,
                                Board.Direction.UP_LEFT,
                                Board.Direction.DOWN_LEFT])

    game = Game([player_1, player_2])
    store = Store(game)

    ai = AI(AI.Difficulty.MEDIUM)
    FormGUI(ai)

    ai_accessor = AIAccessor(store, player_2, ai)
    BoardGUI(store, player_1, game.board)
