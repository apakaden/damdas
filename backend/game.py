from backend.board import Board
from backend.pawn import Pawn
from backend.state.state import State
from backend.state import manager


class Game:
    MAX_NUMBER_OF_STEPS_WITHOUT_CAPTURE = 150

    def __init__(self, players):
        self.players = players
        self.board = Board()
        self.current_state = State({
            Pawn(players[0]): self.board.cells[0][0], Pawn(players[0]): self.board.cells[1][1],
            Pawn(players[0]): self.board.cells[2][0], Pawn(players[0]): self.board.cells[2][1],
            Pawn(players[0]): self.board.cells[4][0], Pawn(players[0]): self.board.cells[3][1],

            Pawn(players[0]): self.board.cells[0][2], Pawn(players[0]): self.board.cells[0][3],
            Pawn(players[0]): self.board.cells[1][2], Pawn(players[0]): self.board.cells[1][3],
            Pawn(players[0]): self.board.cells[2][2], Pawn(players[0]): self.board.cells[2][3],
            Pawn(players[0]): self.board.cells[3][2], Pawn(players[0]): self.board.cells[3][3],
            Pawn(players[0]): self.board.cells[4][2], Pawn(players[0]): self.board.cells[4][3],

            Pawn(players[1]): self.board.cells[0][8], Pawn(players[1]): self.board.cells[1][7],
            Pawn(players[1]): self.board.cells[2][8], Pawn(players[1]): self.board.cells[2][7],
            Pawn(players[1]): self.board.cells[4][8], Pawn(players[1]): self.board.cells[3][7],

            Pawn(players[1]): self.board.cells[0][5], Pawn(players[1]): self.board.cells[0][6],
            Pawn(players[1]): self.board.cells[1][5], Pawn(players[1]): self.board.cells[1][6],
            Pawn(players[1]): self.board.cells[2][5], Pawn(players[1]): self.board.cells[2][6],
            Pawn(players[1]): self.board.cells[3][5], Pawn(players[1]): self.board.cells[3][6],
            Pawn(players[1]): self.board.cells[4][5], Pawn(players[1]): self.board.cells[4][6],
        })

        self.current_player = players[0]
        self.number_of_steps_without_capture = 0

    def apply_movement(self, movement):
        self.current_state = manager.apply_movement(
            self.current_state, movement)

        if movement.pawn_to_remove is None:
            self.number_of_steps_without_capture += 1
        else:
            self.number_of_steps_without_capture = 0

        self.current_player = self.__get_next_player()
        return self.current_state, self.current_player

    def get_valid_moves(self):
        return manager.generate_valid_movements(self.current_state, self.current_player)

    def __get_next_player(self):
        if self.__should_change_player():
            for i in range(0, len(self.players)):
                if self.players[i] == self.current_player:
                    return self.players[(i + 1) % len(self.players)]

        return self.current_player

    def __should_change_player(self):
        return len(manager.generate_valid_movements(self.current_state, self.current_player)) == 0 or not self.current_state.has_captured

    def is_terminal(self):
        return self.current_state.is_terminal() or self.number_of_steps_without_capture > self.MAX_NUMBER_OF_STEPS_WITHOUT_CAPTURE

    def get_winner(self):
        if self.number_of_steps_without_capture > self.MAX_NUMBER_OF_STEPS_WITHOUT_CAPTURE:
            return None
        return manager.get_winner(self.current_state)
