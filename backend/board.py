class Board:

    # Enum sucks, don't use it
    class Direction():
        LEFT = 1
        RIGHT = 2
        UP = 3
        DOWN = 4
        UP_RIGHT = 5
        UP_LEFT = 6
        DOWN_RIGHT = 7
        DOWN_LEFT = 8

        DIRECTIONS = [LEFT, RIGHT, UP, DOWN, UP_RIGHT, UP_LEFT, DOWN_RIGHT, DOWN_LEFT]

    class Cell:

        def __init__(self, row, col):
            self.row = row
            self.col = col
            self.active = True
            self.neighbor = {Board.Direction.LEFT: None,
                             Board.Direction.RIGHT: None,
                             Board.Direction.UP: None,
                             Board.Direction.DOWN: None,
                             Board.Direction.UP_RIGHT: None,
                             Board.Direction.UP_LEFT: None,
                             Board.Direction.DOWN_RIGHT: None,
                             Board.Direction.DOWN_LEFT: None}

        def set_active(self, active):
            self.active = active

        def set_adjacent(self, cell, direction):
            self.neighbor[direction] = cell

        def get_adjacent(self, direction):
            return self.neighbor[direction]

        def __str__(self):
            return "({}, {})".format(self.row, self.col)


    ROWS = 5
    COLUMNS = 9

    def __init__(self):
        self.__init_cells()
        self.__init_cell_neighbors()

    def __init_cells(self):
        self.cells = [[Board.Cell(i, j) for j in range(Board.COLUMNS)] for i in range(Board.ROWS)]
        outer_inactive_cols = [1,7]
        inner_inactive_cols = [0,8]

        for outer_col, inner_col in zip(outer_inactive_cols, inner_inactive_cols):
            self.cells[0][outer_col].set_active(False)
            self.cells[1][inner_col].set_active(False)
            self.cells[3][inner_col].set_active(False)
            self.cells[4][outer_col].set_active(False)


    # TODO: nguli parah? gak ada yang pendekan gitu? :(
    def __init_cell_neighbors(self):
        # LEFT
        # special (col 0-1 and 7-8 + 2,2 and 2,6)
        self.cells[2][2].set_adjacent(self.cells[2][1], Board.Direction.LEFT)
        self.cells[2][1].set_adjacent(self.cells[2][0], Board.Direction.LEFT)

        self.cells[2][8].set_adjacent(self.cells[2][7], Board.Direction.LEFT)
        self.cells[2][7].set_adjacent(self.cells[2][6], Board.Direction.LEFT)

        #---
        for row in range(0, Board.ROWS):
            for col in range(3, 6 + 1):
                self.cells[row][col].set_adjacent(self.cells[row][col-1], Board.Direction.LEFT)

        # RIGHT
        # special (col 0-1 and 7-8 + 2,2 and 2,6)
        self.cells[2][0].set_adjacent(self.cells[2][1], Board.Direction.RIGHT)
        self.cells[2][1].set_adjacent(self.cells[2][2], Board.Direction.RIGHT)

        self.cells[2][6].set_adjacent(self.cells[2][7], Board.Direction.RIGHT)
        self.cells[2][7].set_adjacent(self.cells[2][8], Board.Direction.RIGHT)

        # ---
        for row in range(0, Board.ROWS):
            for col in range(2, 5 + 1):
                self.cells[row][col].set_adjacent(self.cells[row][col + 1], Board.Direction.RIGHT)

        # UP
        # special (col 0-2 and 8-10)
        self.cells[4][0].set_adjacent(self.cells[2][0], Board.Direction.UP)
        self.cells[2][0].set_adjacent(self.cells[0][0], Board.Direction.UP)
        for row in range(2, 3 + 1):
            self.cells[row][1].set_adjacent(self.cells[row-1][1], Board.Direction.UP)

        self.cells[4][8].set_adjacent(self.cells[2][8], Board.Direction.UP)
        self.cells[2][8].set_adjacent(self.cells[0][8], Board.Direction.UP)
        for row in range(2, 3 + 1):
            self.cells[row][7].set_adjacent(self.cells[row-1][7], Board.Direction.UP)

        # ---
        for row in range(1, Board.ROWS):
            for col in range(2, 6 + 1):
                self.cells[row][col].set_adjacent(self.cells[row-1][col], Board.Direction.UP)

        # DOWN
        # special (col 0-2 and 8-10)
        self.cells[0][0].set_adjacent(self.cells[2][0], Board.Direction.DOWN)
        self.cells[2][0].set_adjacent(self.cells[4][0], Board.Direction.DOWN)
        for row in range(1, 2 + 1):
            self.cells[row][1].set_adjacent(self.cells[row + 1][1], Board.Direction.DOWN)

        self.cells[0][8].set_adjacent(self.cells[2][8], Board.Direction.DOWN)
        self.cells[2][8].set_adjacent(self.cells[4][8], Board.Direction.DOWN)
        for row in range(1, 2 + 1):
            self.cells[row][7].set_adjacent(self.cells[row + 1][7], Board.Direction.DOWN)

        # ---
        for row in range(0, Board.ROWS - 1):
            for col in range(2, 6 + 1):
                self.cells[row][col].set_adjacent(self.cells[row + 1][col], Board.Direction.DOWN)

        # UPLEFT
        # special (col 0-2 and 8-10)
        for i in range(4):
            self.cells[4 - i][4 - i].set_adjacent(self.cells[4 - i - 1][4 - i - 1], Board.Direction.UP_LEFT)
        for i in range(4):
            self.cells[4 - i][8 - i].set_adjacent(self.cells[4 - i - 1][8 - i - 1], Board.Direction.UP_LEFT)

        # ---
        for i in range(4):
            self.cells[4 - i][6 - i].set_adjacent(self.cells[4 - i - 1][6 - i - 1], Board.Direction.UP_LEFT)

        # UPRIGHT
        # special (col 0-2 and 8-10)
        for i in range(4):
            self.cells[4 - i][0 + i].set_adjacent(self.cells[4 - i - 1][0 + i + 1], Board.Direction.UP_RIGHT)
        for i in range(4):
            self.cells[4 - i][4 + i].set_adjacent(self.cells[4 - i - 1][4 + i + 1], Board.Direction.UP_RIGHT)

        # ---
        for i in range(4):
            self.cells[4 - i][2 + i].set_adjacent(self.cells[4 - i - 1][2 + i + 1], Board.Direction.UP_RIGHT)

        # DOWNLEFT
        # special (col 0-2 and 8-10)
        for i in range(4):
            self.cells[0 + i][4 - i].set_adjacent(self.cells[0 + i + 1][4 - i - 1], Board.Direction.DOWN_LEFT)
        for i in range(4):
            self.cells[0 + i][8 - i].set_adjacent(self.cells[0 + i + 1][8 - i - 1], Board.Direction.DOWN_LEFT)

        # ---
        for i in range(4):
            self.cells[0 + i][6 - i].set_adjacent(self.cells[0 + i + 1][6 - i - 1], Board.Direction.DOWN_LEFT)

        # DOWNRIGHT
        # special (col 0-2 and 8-10)
        for i in range(4):
            self.cells[0 + i][0 + i].set_adjacent(self.cells[0 + i + 1][0 + i + 1], Board.Direction.DOWN_RIGHT)
        for i in range(4):
            self.cells[0 + i][4 + i].set_adjacent(self.cells[0 + i + 1][4 + i + 1], Board.Direction.DOWN_RIGHT)

        # ---
        for i in range(4):
            self.cells[0 + i][2 + i].set_adjacent(self.cells[0 + i + 1][2 + i + 1], Board.Direction.DOWN_RIGHT)




