from backend.state.manager import generate_valid_movements
from backend.state.manager import apply_movement
from backend.board import Board
from backend.state.manager import get_winner


class AI:
    class Difficulty:
        EASY = "easy"
        MEDIUM = "medium"
        HARD = "hard"

        DIFFICULTIES = [EASY, MEDIUM, HARD]

    _LOOKAHEAD = {Difficulty.EASY: 2, Difficulty.MEDIUM: 4, Difficulty.HARD: 8}
    _INF = 1e9

    def __init__(self, difficulty):
        self.difficulty = difficulty

    def get_next_move(self, state, player):
        score, move = self.__search(state, player, self.__get_other_player(state, player), self._LOOKAHEAD[self.difficulty])
        print("skor:" + str(score))
        return move

    # Mini-Max DFS with pruning
    def __search(self, state, player, other_player, k, turn=True, alpha=None):
        if turn:
            current_player = player
        else:
            current_player = other_player

        movements = generate_valid_movements(state, current_player)

        if k == 0 or len(movements) == 0:
            return self.__util_function(state, player, other_player, turn), None

        best_score = None
        best_move = None

        for move in movements:
            next_state = apply_movement(state, move)
            next_turn = len(generate_valid_movements(next_state, current_player)) == 0

            score, _ = self.__search(next_state, player, other_player, k - (1 if next_turn else 0), turn ^ next_turn, (best_score if next_turn else alpha))

            if best_score is None or (turn and best_score < score) or (not turn and best_score > score):
                best_score = score
                best_move = move

            # alpha-beta pruning
            if alpha is not None and ((turn and best_score >= alpha) or (not turn and best_score <= alpha)):
                break

        return best_score, best_move

    # Heuristic evaluation value of state for a player
    def __util_function(self, state, player, other_player, turn):

        if state.is_terminal():
            return self._INF if get_winner(state) == player else -self._INF

        # we lose if we cannot move
        if len(generate_valid_movements(state, player)) == 0:
            return -self._INF

        # if not our turn and enemy has no move, we win
        if len(generate_valid_movements(state, player)) == 0:
            return self._INF

        ret = 0

        for pawn in state.pawn_cells:
            k = 1
            if pawn.player != player:
                k = -1

            if pawn.is_king():
                # king look for enemies
                position_value = -self._INF
                for pawn2 in state.pawn_cells:
                    if pawn2.player != pawn.player:
                        position_value = max(position_value, -abs(state.pawn_cells[pawn].row - state.pawn_cells[pawn2].row)
                                             - abs(state.pawn_cells[pawn].col - state.pawn_cells[pawn2].col))

                ret += k * (3 + 0.001 * (position_value if position_value != -self._INF else 0))
            else:
                # normal pawn want to be king
                if Board.Direction.RIGHT in pawn.player.default_directions:
                    position_value = state.pawn_cells[pawn].col
                else:
                    position_value = 8 - state.pawn_cells[pawn].col

                ret += k * (1 + 0.01 * position_value)

        return ret

    def __get_other_player(self, state, player):
        for pawn in state.pawn_cells:
            if pawn.player != player:
                return pawn.player
