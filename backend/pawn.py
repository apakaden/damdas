from backend.board import Board


class Pawn:
    def __init__(self, player, king=False):
        self.player = player
        self.king = king
        if king:
            self.allowed_directions = [direction for direction in Board.Direction.DIRECTIONS]
        else:
            self.allowed_directions = player.default_directions

    def is_king(self):
        return self.king

    def promote(self):
        return Pawn(self.player, king=True)

    def get_allowed_directions(self):
        return self.allowed_directions

    def get_player(self):
        return self.player
