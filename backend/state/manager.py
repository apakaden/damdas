from backend.board import Board
from backend.pawn import Pawn
from backend.state.state import State
from backend.state.movement import Movement


def generate_valid_movements(state, player):
    if state.last_pawn is not None and state.last_pawn.player == player and not state.has_captured:
        return []

    standard_movements = []
    capture_movements = []


    for pawn in state.pawn_cells:
        if pawn.player != player or (state.last_pawn is not None and state.last_pawn.player == player and state.last_pawn != pawn):
            continue

        pawn_standard_movements, pawn_capture_movements = generate_pawn_valid_movements(
            state, pawn)
        standard_movements.extend(pawn_standard_movements)
        capture_movements.extend(pawn_capture_movements)

    if capture_movements or (state.last_pawn is not None and state.last_pawn.player == player):
        return capture_movements

    return standard_movements


def generate_pawn_valid_movements(state, pawn):
    standard_movements = []
    capture_movements = []

    pawn_cell = state.pawn_cells[pawn]
    for direction in pawn.get_allowed_directions():
        adjacent = pawn_cell.get_adjacent(direction)
        if adjacent is not None and adjacent in state.taken_positions:
            adjacent_pawn = state.taken_positions[adjacent]
            if adjacent_pawn.player != pawn.player:
                next_adjacent = adjacent.get_adjacent(direction)
                if next_adjacent is not None and next_adjacent not in state.taken_positions:
                    capture_movements.append(
                        Movement(pawn_cell, next_adjacent, adjacent_pawn))
        elif adjacent is not None:
            standard_movements.append(Movement(pawn_cell, adjacent))

    return standard_movements, capture_movements


def apply_movement(state, movement):
    if movement.cell_to in state.taken_positions:
        raise RuntimeError("moving to an occupied cell")

    new_pawn_cells = state.pawn_cells.copy()
    last_pawn = state.taken_positions[movement.cell_from]
    has_captured = False
    if movement.pawn_to_remove is not None:
        new_pawn_cells.pop(movement.pawn_to_remove)
        has_captured = True
    new_pawn_cells[state.taken_positions[movement.cell_from]
                   ] = movement.cell_to

    return __apply_promotions(State(new_pawn_cells, last_pawn, has_captured))


def __apply_promotions(state):
    new_pawn_cells = {}
    for pawn in state.pawn_cells:
        cell = state.pawn_cells[pawn]
        if Board.Direction.RIGHT in pawn.player.default_directions and cell.col > 6:
            # player 1
            pawn = pawn.promote()
        if Board.Direction.LEFT in pawn.player.default_directions and cell.col < 2:
            # player 2
            pawn = pawn.promote()
        new_pawn_cells[pawn] = cell

    return State(new_pawn_cells, state.last_pawn, state.has_captured)


# would not work if state is not terminal state
def get_winner(state):
    for pawn in state.pawn_cells:
        return pawn.player