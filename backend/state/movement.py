
class Movement:

    def __init__(self, cell_from, cell_to, pawn_to_remove=None):
        self.cell_from = cell_from
        self.cell_to = cell_to
        self.pawn_to_remove=pawn_to_remove

    def __eq__(self, other):
        return self.cell_from == other.cell_to \
               and self.cell_to == other.cell_to \
               and self.pawn_to_remove == other.pawn_to_remove

    def __str__(self):
        return "{} -> {}".format(self.cell_from, self.cell_to)
