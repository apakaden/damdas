class State:

    def __init__(self, pawn_cells, last_pawn=None, has_captured=False):
        # key = pawn, value = cell
        self.pawn_cells = pawn_cells

        # key = cell, value = pawn
        self.taken_positions = dict()
        for pawn in self.pawn_cells.keys():
            self.taken_positions[self.pawn_cells[pawn]] = pawn

        self.last_pawn = last_pawn
        self.has_captured = has_captured

    def is_terminal(self):
        sole_player = None
        for pawn in self.pawn_cells:
            if pawn.player != sole_player:
                if sole_player is None:
                    sole_player = pawn.player
                else:
                    return False
        return True