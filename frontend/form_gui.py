from tkinter import *
import tkinter.messagebox
from backend.ai import AI


class FormGUI:
    def __init__(self, ai):
        self.ai = ai
        self.window = Tk()
        self.window.title("Damdas")

        self.frame1 = Frame(self.window, padx=10, pady=10)
        self.frame1.pack()

        self.m = IntVar()
        self.n = IntVar()
        self.k = IntVar()

        self.p1 = StringVar()
        self.p2 = StringVar()

        self.labelm = Label(self.frame1, text="Difficulty: ")
        self.labelm.grid(row=2, column=0, sticky=E)

        self.difficulty = StringVar()
        self.difficulty.set(ai.difficulty)
        R1 = Radiobutton(self.frame1, text="Easy", variable=self.difficulty, value=AI.Difficulty.EASY,
                         command=self.select_difficulty)
        R1.grid(row=2, column=1)

        R2 = Radiobutton(self.frame1, text="Medium", variable=self.difficulty, value=AI.Difficulty.MEDIUM,
                         command=self.select_difficulty)
        R2.grid(row=3, column=1)

        R3 = Radiobutton(self.frame1, text="Hard", variable=self.difficulty, value=AI.Difficulty.HARD,
                         command=self.select_difficulty)
        R3.grid(row=4, column=1)


        self.buttoncreate = Button(
            self.frame1, command=self.window.destroy, text="Create")
        self.buttoncreate.grid(row=8, column=1)

        self.window.mainloop()

    def select_difficulty(self):
        self.ai.difficulty = self.difficulty.get()
