def reduce(game, action):
    if action['type'] == 'MOVE':
        game.apply_movement(action['movement'])


class Store:
    def __init__(self, game):
        self.game = game
        self.subscribers = []

    def subscribe(self, subscriber):
        self.subscribers.append(subscriber)

    def dispatch(self, action):
        reduce(self.game, action)
        for subscriber in self.subscribers:
            subscriber.on_change_state()
