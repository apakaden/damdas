from backend.board import Board
from backend.state.manager import generate_valid_movements
from backend.player import Player
from tkinter import Tk, Canvas, messagebox


class BoardGUI:
    class State:
        def __init__(self, board_gui, valid_movements):
            self.board_gui = board_gui
            self.is_current_turn = False
            self.valid_movements = valid_movements

            self.pick_row = None
            self.pick_col = None
            self.clickable_attribute = None

        def action(self):
            raise NotImplementedError

        def get_clickable_cells(self):
            clickable_cells = []
            for movement in self.valid_movements:
                clickable_cell = getattr(
                    movement, self.clickable_attribute)
                clickable_cells.append(
                    self.board_gui.cells[clickable_cell.row][clickable_cell.col])

            if self.pick_row is not None and self.pick_col is not None:
                clickable_cells.append(
                    self.board_gui.cells[self.pick_row][self.pick_col])
            return clickable_cells

    class PickState(State):
        def __init__(self, board_gui, valid_movements, pick_row, pick_col):
            super().__init__(board_gui, valid_movements)
            self.clickable_cells = []
            self.valid_movements = valid_movements
            self.pick_row = pick_row
            self.pick_col = pick_col
            self.clickable_attribute = 'cell_to'

        def action(self, drop_row, drop_col):
            print('drop', drop_row, drop_col)
            self.board_gui.state = BoardGUI.DropState(self.board_gui)

            if self.pick_row == drop_row and self.pick_col == drop_col:
                return

            movement = next((x for x in self.valid_movements if x.cell_to.row ==
                             drop_row and x.cell_to.col == drop_col), None)

            if movement is None:
                return

            self.board_gui.store.dispatch(
                {'type': 'MOVE', 'movement': movement}
            )

    class DropState(State):
        def __init__(self, board_gui):
            valid_movements = board_gui.store.game.get_valid_moves()
            super().__init__(board_gui, valid_movements)
            self.clickable_attribute = 'cell_from'

        def action(self, pick_row, pick_col):
            print('pick', pick_row, pick_col)

            next_valid_movements = [
                x for x in self.valid_movements if x.cell_from.row == pick_row
                and x.cell_from.col == pick_col
            ]

            self.board_gui.state = BoardGUI.PickState(
                self.board_gui, next_valid_movements, pick_row, pick_col)

    class EnemyState(State):
        def __init__(self, board_gui):
            super().__init__(board_gui, [])

    CELL_SIZE = 80

    def __init__(self, store, player, board):
        self.store = store
        self.store.subscribe(self)

        self.player = player
        self.board = board
        self.init_gui()
        self.state = BoardGUI.DropState(self)
        self.window.mainloop()

    def init_gui(self):
        self.window = Tk()
        self.window.title('Damdas')
        self.canvas = Canvas(self.window, width=self.board.COLUMNS * BoardGUI.CELL_SIZE,
                             height=self.board.ROWS * BoardGUI.CELL_SIZE)
        self.canvas.configure(background="white")
        self.canvas.pack()
        self.cells = [[None for i in range(self.board.COLUMNS)]
                      for j in range(self.board.ROWS)]
        self.create_cells()
        self.apply_state(self.store.game.current_state)

    def hide_cell(self, cell):
        self.canvas.tag_lower(cell)
        # TODO: finish this

    def paint_cell(self, cell, color="white", width=1.0):
        if not cell.active:
            return

        self.canvas.itemconfig(
            self.cells[cell.row][cell.col], fill=color, width=width)

    def apply_state(self, state):
        for i in range(self.board.ROWS):
            for j in range(self.board.COLUMNS):
                self.paint_cell(self.board.cells[i][j])

        for pawn in state.pawn_cells:
            if pawn.player == self.player:
                if pawn.king:
                    self.paint_cell(state.pawn_cells[pawn], "brown")
                else:
                    self.paint_cell(state.pawn_cells[pawn], "red")
            else:
                if pawn.king:
                    self.paint_cell(state.pawn_cells[pawn], "purple")
                else:
                    self.paint_cell(state.pawn_cells[pawn], "blue")

    def on_change_state(self):
        self.apply_state(self.store.game.current_state)

        if self.store.game.is_terminal():
            winner = self.store.game.get_winner()
            if self.player == winner:
                messagebox.showinfo("Selamat", "Anda Menang!")
            if winner is None:
                messagebox.showinfo("Maaf", "Permainan berakhir imbang.")
            else:
                messagebox.showinfo("Maaf", "Anda Kalah")

            return

        if self.store.game.current_player == self.player:
            self.state = BoardGUI.DropState(self)
        else:
            self.state = BoardGUI.EnemyState(self)

    def create_cells(self):
        for i in range(self.board.ROWS):
            for j in range(self.board.COLUMNS):

                if not self.board.cells[i][j].active:
                    continue

                y_coordinate = i * BoardGUI.CELL_SIZE
                x_coordinate = j * BoardGUI.CELL_SIZE

                self.cells[i][j] = self.canvas.create_oval(
                    x_coordinate+20, y_coordinate+20, x_coordinate + BoardGUI.CELL_SIZE - 20, y_coordinate + BoardGUI.CELL_SIZE - 20, width=1)
                for direction in Board.Direction.DIRECTIONS:
                    adjacent = self.board.cells[i][j].get_adjacent(direction)
                    if adjacent is not None:
                        y_coordinate2 = adjacent.row * BoardGUI.CELL_SIZE
                        x_coordinate2 = adjacent.col * BoardGUI.CELL_SIZE
                        self.canvas.tag_lower(self.canvas.create_line(
                            x_coordinate + 40, y_coordinate + 40, x_coordinate2 + 40, y_coordinate2 + 40, fill="black"))

                self.paint_cell(self.board.cells[i][j])
                self.canvas.tag_bind(
                    self.cells[i][j], '<ButtonPress-1>', lambda event, row=i, col=j: self.click_cell(event, row, col))
                self.canvas.tag_bind(
                    self.cells[i][j], '<Enter>', lambda event, row=i, col=j: self.enter_cell(event, row, col))
                self.canvas.tag_bind(
                    self.cells[i][j], '<Leave>', lambda event, row=i, col=j: self.leave_cell(event, row, col))

    def click_cell(self, event, row, col):
        if self.cells[row][col] in self.state.get_clickable_cells():
            self.state.action(row, col)

    def enter_cell(self, event, row, col):
        if self.cells[row][col] in self.state.get_clickable_cells():
            self.canvas.itemconfig(
                self.cells[row][col], width=5.0)

    def leave_cell(self, event, row, col):
        if row != self.state.pick_row or col != self.state.pick_col:
            self.canvas.itemconfig(
                self.cells[row][col], width=1.0)
