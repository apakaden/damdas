from backend.ai import AI


class AIAccessor:
    def __init__(self, store, player, ai):
        self.store = store
        self.store.subscribe(self)

        self.player = player
        self.ai = ai

    def on_change_state(self):
        if self.store.game.is_terminal():
            return

        if self.store.game.current_player == self.player:
            movement = self.ai.get_next_move(
                self.store.game.current_state, self.player)
            self.store.dispatch(
                {'type': 'MOVE', 'movement': movement}
            )
