from tkinter import *
from enum import Enum


class Position:
    def __init__(self, y, x):
        self.y = y
        self.x = x

    def __eq__(self, other):
        return self.y == other.y and self.x == other.x

    def __str__(self):
        return "({}, {})".format(self.y, self.x)

    def __hash__(self):
        return hash(str(self))


class Movement:
    def __init__(self, pick_position, drop_position, pick_pawn, capture_position=None, capture_pawn=None, is_reverse=False):
        self.pick_position = pick_position
        self.drop_position = drop_position
        self.pick_pawn = pick_pawn
        self.capture_position = capture_position
        self.capture_pawn = capture_pawn
        self.is_reverse = is_reverse

    def reverse(self):
        return Movement(self.drop_position, self.pick_position, self.pick_pawn, self.capture_position, self.capture_pawn, not self.is_reverse)

    def __str__(self):
        return "from {} to {}".format(str(self.pick_position), str(self.drop_position))


class Player:
    def __init__(self, color, name):
        self.color = color
        self.name = name
        self.direction = 1 if color == "red" else -1


class Pawn:
    def __init__(self, color, position):
        self.color = color
        self.position = position
        self.king = False
        self.direction = 1 if color == "red" else -1

    def set_king(self):
        self.king = True


class Cell:
    def __init__(self, position):
        self.position = position
        self.disabled = False
        self.neighbors = {}

    def get_neighbors(self, directions=[-1, 0, 1]):
        ret = []
        for direction in directions:
            ret += [neighbor for neighbor in self.neighbors[direction]]
        return ret


class Board:
    ROW = 5
    COLUMN = 9

    def __init__(self):
        self.init_cells()
        self.init_cell_adjacencies()

    def init_cells(self):
        self.cells = [[Cell(Position(i, j)) for j in range(Board.COLUMN)]
                      for i in range(Board.ROW)]

        self.cells[1][0].disabled = True
        self.cells[3][0].disabled = True

        self.cells[0][1].disabled = True
        self.cells[2][1].disabled = True
        self.cells[4][1].disabled = True

        self.cells[0][7].disabled = True
        self.cells[2][7].disabled = True
        self.cells[4][7].disabled = True

        self.cells[1][8].disabled = True
        self.cells[3][8].disabled = True

    def init_cell_adjacencies(self):
        self.cells[0][0].neighbors[-1] = []
        self.cells[0][0].neighbors[0] = [(self.cells[2][0], self.cells[4][0])]
        self.cells[0][0].neighbors[1] = [(self.cells[1][1], self.cells[2][2])]

        self.cells[2][0].neighbors[-1] = []
        self.cells[2][0].neighbors[0] = [
            (self.cells[0][0], None), (self.cells[4][0], None)]
        self.cells[2][0].neighbors[1] = [
            (self.cells[1][1], None), (self.cells[3][1], None)]

        self.cells[4][0].neighbors[-1] = []
        self.cells[4][0].neighbors[0] = [(self.cells[2][0], self.cells[0][0])]
        self.cells[4][0].neighbors[1] = [(self.cells[3][1], self.cells[2][2])]

        self.cells[1][1].neighbors[-1] = [(self.cells[0]
                                           [0], None), (self.cells[2][0], None)]
        self.cells[1][1].neighbors[0] = [(self.cells[3][1], None)]
        self.cells[1][1].neighbors[1] = [(self.cells[2][2], self.cells[3][3])]

        self.cells[3][1].neighbors[-1] = [(self.cells[2]
                                           [0], None), (self.cells[4][0], None)]
        self.cells[3][1].neighbors[0] = [(self.cells[1][1], None)]
        self.cells[3][1].neighbors[1] = [(self.cells[2][2], self.cells[1][3])]

        self.cells[2][2].neighbors[-1] = [
            (self.cells[1][1], self.cells[0][0]), (self.cells[3][1], self.cells[4][0])]
        self.cells[2][2].neighbors[0] = []
        self.cells[2][2].neighbors[1] = []

    def get_cell(self, position):
        return self.cells[position.y][position.x]


# only return/receive position from GUI class
class Game:
    def __init__(self):
        self.players = [Player("red", "Player 1"), Player("blue", "Player 2")]
        self.board = Board()
        self.step = 0
        self.turn = 0
        self.histories = []
        self.pawns = {}
        self.pawns["red"] = {}
        self.pawns["blue"] = {}
        self.init_pawns()

    def init_pawns(self):
        self.pawns["red"][Position(0, 0)] = Pawn("red", Position(0, 0))
        self.pawns["blue"][Position(2, 0)] = Pawn("blue", Position(2, 0))
        self.pawns["red"][Position(4, 0)] = Pawn("red", Position(4, 0))

    @property
    def current_player_color(self):
        return self.players[self.turn].color

    @property
    def current_enemy_color(self):
        return self.players[1 - self.turn].color

    def get_pick_position_candidates(self):
        return list(self.pawns[self.current_player_color].keys())

    def get_capture_move_candidates(self):
        move_candidates = []
        for pick_position in self.get_pick_position_candidates():
            pick_cell = self.board.get_cell(pick_position)

            for capture_cell, drop_cell in pick_cell.get_neighbors():
                if drop_cell is None:
                    continue

                capture_position = capture_cell.position
                drop_position = drop_cell.position
                pick_pawn = self.pawns[self.current_player_color][pick_position]

                if self.is_valid_capture(pick_position, capture_position, drop_position):
                    capture_pawn = self.pawns[self.current_enemy_color][capture_position]
                    move_candidates.append(Movement(
                        pick_position, drop_position, pick_pawn, capture_position, capture_pawn))

        return move_candidates

    def get_basic_move_candidates(self):
        move_candidates = []
        for pick_position in self.get_pick_position_candidates():
            pick_pawn = self.pawns[self.current_player_color][pick_position]
            pick_cell = self.board.get_cell(pick_position)

            for drop_cell, _ in pick_cell.get_neighbors():
                drop_position = drop_cell.position

                if self.is_cell_available(drop_cell):
                    move_candidates.append(
                        Movement(pick_position, drop_position, pick_pawn))

        return move_candidates

    def get_move_candidates(self):
        move_candidates = self.get_capture_move_candidates()
        if len(move_candidates) > 0:
            return move_candidates
        return self.get_basic_move_candidates()

    def is_valid_capture(self, pick_position, capture_position, drop_position):
        return self.is_cell_available(self.board.get_cell(drop_position)) and\
            capture_position in self.pawns[self.current_enemy_color]

    def is_cell_available(self, cell):
        return not cell.disabled and\
            cell.position not in self.pawns[self.current_enemy_color] and\
            cell.position not in self.pawns[self.current_player_color]

    def move_pawn(self, movement):
        self.pawns[self.current_player_color].pop(movement.pick_position)
        movement.pick_pawn.position = movement.drop_position
        self.pawns[self.current_player_color][movement.drop_position] = movement.pick_pawn

    def capture_pawn(self, movement):
        self.pawns[self.current_enemy_color].pop(movement.capture_position)

    def restore_pawn(self, movement):
        self.pawns[movement.capture_pawn.color][movement.capture_position] = movement.capture_pawn

    def move(self, movement):
        self.move_pawn(movement)
        if movement.capture_pawn is not None:
            self.capture_pawn(movement)
        self.histories.append(movement)
        self.step += 1
        self.turn = 1 - self.turn

    def undo_move(self):
        self.step -= 1
        self.turn = 1 - self.turn
        movement = self.histories.pop().reverse()

        self.move_pawn(movement)
        if movement.capture_pawn is not None:
            self.restore_pawn(movement)


class BoardGUI:
    CELL_SIZE = 50

    class State:
        pass

    class PickState(State):
        def __init__(self, board_gui, move_candidates, pick_position):
            print("Picked")
            self.board_gui = board_gui
            self.move_candidates = move_candidates
            self.pick_position = pick_position
            self.paint_move_candidates("green")

        def paint_move_candidates(self, color):
            for move in self.move_candidates:
                self.board_gui.paint_cell(move.drop_position, color)

        def action(self, drop_position):
            if self.pick_position == drop_position:
                self.paint_move_candidates("white")
                self.board_gui.state = BoardGUI.DropState(self.board_gui)

            selected_move = [
                move for move in self.move_candidates if move.drop_position == drop_position]
            if len(list(selected_move)) > 0:
                selected_move = selected_move[0]
                self.paint_move_candidates("white")
                self.board_gui.move(selected_move)
                self.board_gui.state = BoardGUI.DropState(self.board_gui)

                self.board_gui.game.undo_move()

    class DropState(State):
        def __init__(self, board_gui):
            print("Dropped")
            self.board_gui = board_gui
            self.move_candidates = self.board_gui.game.get_move_candidates()
            print("move_candidates:", ", ".join(
                [str(x) for x in self.move_candidates]))

        def action(self, pick_position):
            move_candidates = [
                move for move in self.move_candidates if move.pick_position == pick_position]
            if len(move_candidates) > 0:
                self.board_gui.state = BoardGUI.PickState(
                    self.board_gui, move_candidates, pick_position)

    def __init__(self):
        self.game = Game()
        self.state = BoardGUI.DropState(self)

        self.init_gui()

    def init_gui(self):
        self.window = Tk()
        self.window.title('Damdas')
        self.canvas = Canvas(self.window, width=self.game.board.COLUMN * BoardGUI.CELL_SIZE,
                             height=self.game.board.ROW * BoardGUI.CELL_SIZE)
        self.canvas.pack()
        self.cells = [[0 for i in range(self.game.board.COLUMN)]
                      for j in range(self.game.board.ROW)]
        self.create_cells()
        self.init_pawn_cells()
        self.window.mainloop()

    def init_pawn_cells(self):
        for position in self.game.pawns["red"].keys():
            self.paint_cell(position, "red")
        for position in self.game.pawns["blue"].keys():
            self.paint_cell(position, "blue")

    def paint_cell(self, position, color="white"):
        cell = self.game.board.get_cell(position)
        if cell.disabled:
            color = "grey"

        self.canvas.itemconfig(
            self.cells[position.y][position.x], fill=color)

    def create_cells(self):
        for i in range(self.game.board.ROW):
            for j in range(self.game.board.COLUMN):
                y_coordinate = i * BoardGUI.CELL_SIZE
                x_coordinate = j * BoardGUI.CELL_SIZE
                self.cells[i][j] = self.canvas.create_rectangle(
                    x_coordinate, y_coordinate, x_coordinate + BoardGUI.CELL_SIZE, y_coordinate + BoardGUI.CELL_SIZE, width=1)
                for direction in Board.Directions:

                self.paint_cell(Position(i, j))
                self.canvas.tag_bind(
                    self.cells[i][j], '<ButtonPress-1>', lambda event, y=i, x=j: self.state.action(Position(y, x)))
                self.canvas.tag_bind(
                    self.cells[i][j], '<Enter>', lambda event, y=i, x=j: self.enter_cell(Position(y, x)))
                self.canvas.tag_bind(
                    self.cells[i][j], '<Leave>', lambda event, y=i, x=j: self.leave_cell(Position(y, x)))

    def enter_cell(self, position):
        if position in self.game.pawns[self.game.current_player_color]:
            self.canvas.itemconfig(
                self.cells[position.y][position.x], outline="green", width=10.0)

    def leave_cell(self, position):
        if position in self.game.pawns[self.game.current_player_color]:
            self.canvas.itemconfig(
                self.cells[position.y][position.x], outline="black", width=1.0)

    def move(self, movement):
        self.paint_cell(movement.pick_position)
        if movement.capture_position is not None:
            self.paint_cell(movement.capture_position)
        self.paint_cell(movement.drop_position, movement.pick_pawn.color)

        print("movement", movement)
        self.game.move(movement)

if __name__ == "__main__":
    BoardGUI()
